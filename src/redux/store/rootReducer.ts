import {combineReducers} from 'redux'
import columnSlice from './column/columnSlice'
import taskSlice from './task/taskSlice'
import userSlice from './user/userSlice'
import commentSlice from './comment/commentSlice'

const rootReducer = combineReducers({
    user: userSlice,
    columns: columnSlice,
    tasks: taskSlice,
    comments: commentSlice,
})

export {rootReducer}
