import {createSlice} from '@reduxjs/toolkit'
import {DEFAULT_AUTHOR_NAME} from '../../../dto/constants'
import {UserType} from '../../../dto/types'

const initialState: UserType = {
    input: DEFAULT_AUTHOR_NAME
}

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setUser: (state, action) => {
                return state = action.payload
        },
        changeUser: (state, action) => {
            return state = action.payload
        }
    }
})

export default userSlice.reducer
export const {setUser, changeUser} = userSlice.actions