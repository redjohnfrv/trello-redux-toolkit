import {createSelector} from 'reselect'
import {RootState} from '..'

export const selectTasks = (state: RootState) => state.tasks

export const selectTaskIdByColumnId = createSelector(
  (state: RootState) => state.tasks,
  (state: RootState, columnId: string) => columnId,
  (tasks, columnId) =>
    tasks.filter(task => task.columnId === columnId).map(task => task.id)
)
