import {TaskType} from '../../../dto/types'
import {DEFAULT_TITLE} from '../../../dto/constants'
import {createSlice} from '@reduxjs/toolkit'

const initialState: TaskType[] = []

const taskSlice = createSlice({
    name: 'tasks',
    initialState,
    reducers: {
        addNewTask: (state, action) => {
            if (!action.payload.title) {
                action.payload.title = DEFAULT_TITLE
                state.push(action.payload)
            } else {
                state.push(action.payload)
            }
        },
        removeOldTask: (state, action) => {
            return state.filter(task => task.id !== action.payload)
        },
        changeTaskDescription: (state, action) => {
            state.map(task => {
                if (task.id === action.payload.id) {
                    task.description = action.payload.description
                }
                return state
            })
        },
        changeTaskTitle: (state, action) => {
           state.map(task => {
               if (task.id === action.payload.id) {
                   task.title = action.payload.title
               }
               return state
           })
        },
        changeTaskStatus: (state, action) => {
            state.map(task => {
                if (task.columnId === action.payload.id) {
                    task.columnId = action.payload.newStatus
                }
                return state
            })
        }
    }
})

export default taskSlice.reducer
export const {
    addNewTask,
    removeOldTask,
    changeTaskDescription,
    changeTaskTitle,
    changeTaskStatus,
} = taskSlice.actions