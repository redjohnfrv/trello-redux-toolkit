import {createSlice} from '@reduxjs/toolkit'
import {ColumnType} from '../../../dto/types'

const initialState: ColumnType[] = [
    { title: "TODO", id: "1" },
    { title: "In Progress", id: "2" },
    { title: "Testing", id: "3" },
    { title: "Done", id: "4" },
]

const columnSlice = createSlice({
    name: 'column',
    initialState,
    reducers: {
        setTitle: (state) => {
            return state
        }
    }
})

export default columnSlice.reducer
export const {setTitle} = columnSlice.actions