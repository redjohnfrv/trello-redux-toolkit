import {RootState} from '..'
import {createSelector} from 'reselect'

export const selectComments = (state: RootState) => state.comments

export const selectCommentsById = createSelector(
    (state: RootState) => state.comments,
    (state: RootState, id: string) => id,
    (comment, id) => comment.filter(comment => comment.taskId === id)
)