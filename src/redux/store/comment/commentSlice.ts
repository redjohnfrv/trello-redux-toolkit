import {CommentType} from '../../../dto/types'
import {DEFAULT_COMMENT_BODY} from '../../../dto/constants'
import {createSlice} from '@reduxjs/toolkit'

const initialState: CommentType[] = []

const commentSlice = createSlice({
    name: 'comments',
    initialState,
    reducers: {
        addNewComment: (state, action) => {
            if (!action.payload.body) {
                action.payload.body = DEFAULT_COMMENT_BODY
            }
            state.push(action.payload)
        },
        removeCurrentComment: (state, action) => {
            return state.filter(comment => comment.id !== action.payload)
        },
        removeAllComments: (state, action) => {
            return state.filter(comment => comment.taskId !== action.payload)
        },
        changeCurrentComment: (state, action) => {
            state.map(comment => {
                if (comment.id === action.payload.id) {
                    comment.body = action.payload.body
                }
                return state
            })
        }
    }
})

export default commentSlice.reducer
export const {
    addNewComment,
    removeCurrentComment,
    removeAllComments,
    changeCurrentComment,
} = commentSlice.actions