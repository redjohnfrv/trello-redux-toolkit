import React, {useCallback, useEffect} from 'react'
import styled from 'styled-components'
import {images} from '../../assets/images/images'
import {selectCommentsById} from '../../redux/store/comment/selectors'
import {Popup} from '../../ui/Popup'
import {Task} from './Task'
import {useSwitcher} from '../../hooks/useSwitcher'
import {useDispatch, useSelector} from 'react-redux'
import {useFindTask} from '../../hooks/useFindTask'
import {removeOldTask} from '../../redux/store/task/taskSlice'
import {removeAllComments} from '../../redux/store/comment/commentSlice'
import {RootState} from '../../redux/store'

type Props = {
    taskId: string
}

export function TaskPreview({taskId}: Props) {
    const dispatch = useDispatch()
    const taskById = useFindTask(taskId)
    const commentsIds = useSelector((state: RootState) =>
        selectCommentsById(state, taskId)
    )
    const showPopup = useSwitcher()
    const taskRemove = () => {
        dispatch(removeOldTask(taskById.id))
        dispatch(removeAllComments(taskId))
    }

    const closePopupOnEsc = useCallback((e: KeyboardEvent) => {
        const {key} = e
        if (key === 'Escape') showPopup.off()
    }, [showPopup])
    useEffect(() => {
        if (showPopup.isOn) document.addEventListener('keydown', closePopupOnEsc)
        return () => document.removeEventListener('keydown', closePopupOnEsc)
    }, [closePopupOnEsc, showPopup.isOn])


    return (
        <>
            <TaskPreviewWrapper onClick={() => showPopup.on()}>
                <span>{taskById.title}</span>
                <TaskRemoveButton onClick={taskRemove} />
                <TaskCommentsCounterWrapper>
                    {commentsIds.length}
                </TaskCommentsCounterWrapper>
            </TaskPreviewWrapper>
            {showPopup.isOn &&
                <Popup popupClose={() => showPopup.off()}>
                    <Task taskId={taskId} />
                </Popup>}
        </>
    )
}

const TaskPreviewWrapper = styled.div`
  position: relative;
  box-sizing: border-box;
  min-width: 100px;
  width: calc(100% - 8px);
  height: 56px;
  margin-bottom: 12px;
  padding: 0 16px 0 8px;
  line-height: 36px;
  text-align: left;
  background: #fff;
  border-radius: 15px;
  cursor: pointer;
  
  & span {
    display: inline-block;
    width: 100%;
    font-weight: bold;
    white-space: nowrap;
    overflow: auto;
    text-overflow: ellipsis;
  }
`

const TaskRemoveButton = styled.div`
  position: absolute;
  top: 20px;
  left: calc(100% + 8px);
  width: 20px;
  height: 20px;
  background: url(${images.trash}) center / cover no-repeat;
  cursor: grab;
  
  &:hover {
    filter: blur(.5px);
  }
`

const TaskCommentsCounterWrapper = styled.div`
  position: absolute;
  top: 24px;
  left: 28px;
  font-size: 12px;

  &:before {
    content: '';
    position: absolute;
    top: 10px;
    left: -20px;
    width: 15px;
    height: 15px;
    background: url(${images.comment}) center / cover no-repeat;
  }
`