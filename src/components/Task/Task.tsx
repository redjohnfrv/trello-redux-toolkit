import React, {ChangeEvent} from 'react'
import styled from 'styled-components'
import {variables} from '../../assets/styles/variables'
import {DEFAULT_DESCRIPTION, DEFAULT_TITLE} from '../../dto/constants'
import {Comment} from '../Comment/Comment'
import {FFSelect} from '../../ui/FinalForm/FFSelect'
import {FFInput} from '../../ui/FinalForm/FFInput'
import {FFTextArea} from '../../ui/FinalForm/FFTextArea'
import {useDispatch, useSelector} from 'react-redux'
import {useSwitcher} from '../../hooks/useSwitcher'
import {useFindTask} from '../../hooks/useFindTask'
import {useFindComments} from '../../hooks/useFindComments'
import {addNewComment} from '../../redux/store/comment/commentSlice'
import {selectUser} from '../../redux/store/user/userSelector'
import {
    changeTaskDescription,
    changeTaskStatus,
    changeTaskTitle
} from '../../redux/store/task/taskSlice'
import {InputValueType} from '../../dto/types'

type Props = {
    taskId: string
}

export function Task({taskId}: Props) {
    const dispatch = useDispatch()
    const showAddComment = useSwitcher()
    const showChangeDescription = useSwitcher()
    const showChangeTitle = useSwitcher()
    const userName = useSelector(selectUser).input
    const taskById = useFindTask(taskId)
    const taskComments = useFindComments(taskId)

    const createComment = (value: string, taskId: string) => {
        const newComment = {
            id: String(Date.now()),
            taskId: taskId,
            body: value,
            author: userName,
        }
        dispatch(addNewComment(newComment))
        showAddComment.off()
    }

    const changeStatus = (e: ChangeEvent<HTMLSelectElement>) => {
        const newStatus = Number(e.target.value) + 1
        dispatch(changeTaskStatus({id: taskById!.columnId, newStatus: String(newStatus)}))
    }

    const changeDescription = (value: InputValueType) => {
        if (!value.input) value.input = DEFAULT_DESCRIPTION
        dispatch(changeTaskDescription({id: taskId, description: value.input}))
        showChangeDescription.off()
    }

    const changeTitle = (value: InputValueType) => {
        if (!value.input) value.input = DEFAULT_TITLE
        dispatch(changeTaskTitle({id: taskId, title: value.input}))
        showChangeTitle.off()
    }

    return (
        <>
            <TaskTitleWrapper onClick={() => showChangeTitle.on()}>
                <h1>{taskById.title}</h1>
                <span>(Автор: {taskById.author})</span>
                {showChangeTitle.isOn &&
                    <FFTextArea
                        description={taskById.title}
                        addFunction={changeTitle}
                        maxLength="15"
                    />
                }
            </TaskTitleWrapper>
            <TaskBodyWrapper>
                <TaskDescriptionWrapper>
                    <TaskDescriptionBody onClick={() => showChangeDescription.on()}>
                        <span>{taskById.description || DEFAULT_DESCRIPTION}</span>
                    </TaskDescriptionBody>
                    {showChangeDescription.isOn &&
                        <FFTextArea
                            description={taskById.description}
                            addFunction={changeDescription}
                        />
                    }
                </TaskDescriptionWrapper>
                <TaskStatusWrapper>
                    <FFSelect status={taskById.columnId}
                              changeStatus={changeStatus}
                    />
                </TaskStatusWrapper>
            </TaskBodyWrapper>
            <TaskCommentsWrapper>
                <h2>comments</h2>
                {taskComments.map((comment, id) => {
                    return <Comment key={id} commentId={comment.id} taskId={taskId}/>
                })}
                {showAddComment.isOn &&
                    <FFInput
                        id={taskId}
                        addFunction={createComment}
                    />
                }
                <span
                    onClick={() => showAddComment.on()}
                ><span>+</span> Add comment</span>
            </TaskCommentsWrapper>
        </>
    )
}

const TaskTitleWrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: left;
  align-items: center;
  width: max-content;
  margin-bottom: 36px;
  cursor: pointer;
  
  & span {
    margin-left: 8px;
    white-space: nowrap;
    font-size: 24px;
  }
  
  & textarea {
    position: absolute;
    top: 0;
    left: 0;
    width: 300px;
    height: 28px;
  }
  
  & button {
    position: absolute;
    top: 100%;
    left: 0;
    width: 45px;
    height: 80%;
    background: ${variables.defaultYellow};
    border: 1px solid ${variables.defaultBlack};
    border-radius: 3px;
  }
`

const TaskBodyWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 24px;
`

const TaskDescriptionWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  width: 70%;
  margin-bottom: 12px;
  
  & h2 {
    color: ${variables.lightGrey};
    text-transform: uppercase;
    font-size: 16px;
    margin-bottom: 0;
  }
  
  & textarea {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }
  
  & button {
    position: absolute;
    top: calc(100% + 10px);
    left: 0;
    width: calc(100% + 6px);
    background: ${variables.defaultYellow};
    border: 1px solid ${variables.defaultBlack};
    border-radius: 3px;
    opacity: .6;
    
    &:hover {
      opacity: 1;
    }
  }
`
const TaskStatusWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 30%;
`

const TaskCommentsWrapper = styled.div`
  text-align: left;
  
  & h2 {
    color: ${variables.lightGrey};
    text-transform: uppercase;
    font-size: 16px;
    margin-bottom: 0;
  }
  
  & span {
    display: inline-block;
    width: max-content;
    white-space: nowrap;
    font-size: ${variables.textNormal};
    color: ${variables.defaultBlack};
    -webkit-text-stroke: unset;
    cursor: pointer;
    margin-top: 10px;
    
    & span {
      position: relative;
      top: 2px;
      font-size: 36px;
      font-weight: bold;
      color: ${variables.defaultYellow};
      -webkit-text-stroke: .5px ${variables.defaultBlack};
    }
  }
`
const TaskDescriptionBody = styled.div`
  position: relative;
  text-align: left;
  
  & span {
    width: 350px;
    padding-left: 8px;
    text-align: left;
    cursor: pointer;
  }
  
  & textarea {
    position: absolute;
    left: 0;
    top: 0;
    width: 350px;
    height: 60px;
    overflow: scroll;
  }
`