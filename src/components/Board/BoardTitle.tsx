import React from 'react'
import styled from 'styled-components'
import {images} from '../../assets/images/images'
import {useSelector} from 'react-redux'
import {selectUser} from '../../redux/store/user/userSelector'

export function BoardTitle() {
    const author = useSelector(selectUser).input
    return (
        <BoardTitleWrapper>
            <h1>Hello, {author}!</h1>
        </BoardTitleWrapper>
    )
}

const BoardTitleWrapper = styled.div`
  width: 100%;
  max-width: 200px;
  margin-bottom: 36px;
  background: url(${images.paint}) center/cover no-repeat;
`