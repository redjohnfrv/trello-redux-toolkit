import React from 'react'
import styled from 'styled-components'
import {Container} from '../../ui/Container'
import {Column} from '../Column/Column'
import {BoardTitle} from './BoardTitle'
import {useSelector} from 'react-redux'
import {selectColumns} from '../../redux/store/column/selectors'

export function Board() {
    const columnsIds = useSelector(selectColumns)
    return (
        <Container>
            <BoardTitle />
            <ColumnWrapper>
                {columnsIds.map((columnId: string) => {
                    return <Column columnId={columnId} key={columnId} />
                })}
            </ColumnWrapper>
        </Container>
    )
}

const ColumnWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`