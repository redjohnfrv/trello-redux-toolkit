import React from 'react'
import styled from 'styled-components'
import {images} from '../../assets/images/images'
import {variables} from '../../assets/styles/variables'
import {useFindTask} from '../../hooks/useFindTask'
import {useFindComments} from '../../hooks/useFindComments'
import {useDispatch} from 'react-redux'
import {changeCurrentComment, removeCurrentComment} from '../../redux/store/comment/commentSlice'
import {useSwitcher} from '../../hooks/useSwitcher'
import {FFTextArea} from '../../ui/FinalForm/FFTextArea'
import {InputValueType} from '../../dto/types'

type Props = {
    taskId: string
    commentId: string
}

export function Comment({taskId, commentId}: Props) {
    const dispatch = useDispatch()
    const showChangeComment = useSwitcher()
    const taskById = useFindTask(taskId)
    const author = taskById.author
    const comment = useFindComments(taskId).find(comment => comment.id === commentId)!

    const removeComment = (id: string) => {
        dispatch(removeCurrentComment(id))
    }

    const changeComment = (value: InputValueType) => {
        dispatch(changeCurrentComment({id: commentId, body: value.input}))
        showChangeComment.off()
    }

    return (
        <CommentWrapper onClick={() => showChangeComment.on()}>
            <p>{comment.body}</p>
            <CommentAuthorWrapper>
                {author}:
            </CommentAuthorWrapper>
            <CommentRemoveButton onClick={() => removeComment(commentId)} />
            {showChangeComment.isOn &&
                <FFTextArea
                    description={comment.body}
                    addFunction={changeComment}
                />
            }
        </CommentWrapper>
    )
}

const CommentWrapper = styled.div`
  position: relative;
  padding: 4px 8px;
  margin-bottom: 8px;
  background: #e3e2e2;
  border-radius: 5px;
  cursor: pointer;

  & p {
    max-width: 85%;  
  }
  
  & textarea {
    position: absolute;
    top: 0;
    left: 0;
    width: calc(100% - 6px);
    height: 100%;
    z-index: 15;
  }

  & button {
    position: absolute;
    top: calc(100% - 20px);
    left: 0;
    width: calc(100%);
    height: 25px;
    background: ${variables.defaultYellow};
    border: 1px solid ${variables.defaultBlack};
    border-radius: 3px;
    opacity: 1;
    z-index: 15;
  }
`

const CommentAuthorWrapper = styled.span`
  position: absolute;
  top: -4px;
  left: 8px;
  font-size: 12px !important;
  z-index: 10;
`

const CommentRemoveButton = styled.div`
  position: absolute;
  top: 15px;
  left: 94%;
  width: 15px;
  height: 15px;
  background: url(${images.trash}) center / cover no-repeat;
  cursor: grab;
`
