import React from 'react'
import styled from 'styled-components'
import {variables} from '../../assets/styles/variables'
import {EMPTY_STRING} from '../../dto/constants'
import {selectColumnById} from '../../redux/store/column/selectors'
import {selectTaskIdByColumnId} from '../../redux/store/task/selectors'
import {TaskPreview} from '../Task/TaskPreview'
import {useSwitcher} from '../../hooks/useSwitcher'
import {useDispatch, useSelector} from 'react-redux'
import {FFInput} from '../../ui/FinalForm/FFInput'
import {addNewTask} from '../../redux/store/task/taskSlice'
import {selectUser} from '../../redux/store/user/userSelector'
import { RootState } from '../../redux/store'

type Props = {
    columnId: string
}

export function Column({columnId}: Props) {
    const dispatch = useDispatch()
    const showInput = useSwitcher()
    const userName = useSelector(selectUser).input
    const {title} = useSelector((state: RootState) =>
        selectColumnById(state, columnId)
    )!
    const taskIds = useSelector((state: RootState) =>
        selectTaskIdByColumnId(state, columnId)
    )
    const createTask = (title: string, columnId: string): void => {
        const newTask = {
            id: String(Date.now()),
            columnId,
            title: title,
            description: EMPTY_STRING,
            author: userName,
        }
        dispatch(addNewTask(newTask))
        showInput.off()
    }

    return (
        <ColumnPreviewTaskWrapper>
            <h2>{title}</h2>
            {taskIds.map((taskId: string) => {
                return <TaskPreview taskId={taskId} key={taskId} />
            })}
            {showInput.isOn &&
                <FFInput
                    id={columnId}
                    addFunction={createTask}
                />
            }
            <ShowInputButton onClick={() => showInput.toggle()}>ADD TASK</ShowInputButton>
        </ColumnPreviewTaskWrapper>
    )
}

const ColumnPreviewTaskWrapper = styled.div`
  width: 180px;
  min-width: 120px;
  min-height: 250px;
  margin-bottom: 10px;
  background: ${variables.lightGrey};
  border-radius: 15px;
  padding: ${variables.defaultPadding};
  text-align: center;
  
  &:not(:last-child) {
    margin-right: 10px;
  }
  
  & h2 {
    background: ${variables.theme};
    margin-bottom: 24px;
    padding: 8px;
    border-radius: 15px;
    font-style: italic;
  }
`

export const ShowInputButton = styled.div`
  margin-top: 8px;
  font-size: 18px;
  text-align: right;
  color: ${variables.defaultYellow};
  cursor: pointer;
  opacity: .6;
  
  &:hover {
    opacity: 1;
  }
`