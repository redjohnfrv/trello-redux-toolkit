export type UserType = {
    input: string
}

export type CommentType = {
    id: string
    taskId: string
    body: string
    author: string
}

export type TaskType = {
    id: string
    columnId: string
    title: string
    description: string
    author: string
    status: string
}

export type ColumnType = {
    id: string
    title: string
}


export type InputValueType = {
    input: string
}