export const EMPTY_STRING = ''
export const DEFAULT_TITLE = 'Unnamed'
export const DEFAULT_DESCRIPTION = 'Click here to add description...'
export const DEFAULT_AUTHOR_NAME = 'user'
export const DEFAULT_COMMENT_BODY = 'empty comment...'
export const WILL_NEVER_HAPPENED = 'will never happened'

export const TO_DO = 'TODO'
export const IN_PROGRESS = 'In progress'
export const TESTING = 'Testing'
export const DONE = 'Done'

export enum EStatus {
    'TODO',
    'In progress',
    'Testing',
    'Done',
}