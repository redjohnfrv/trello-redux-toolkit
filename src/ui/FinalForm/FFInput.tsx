import React from 'react'
import {Form, Field} from 'react-final-form'
import {variables} from '../../assets/styles/variables'
import styled from 'styled-components'
import {InputValueType} from '../../dto/types'

type Props = {
    id: string
    addFunction: (title: string, id: string) => void
}

export function FFInput({addFunction, id}: Props) {

    const onSubmit = (value: InputValueType) => {
        const inputValue = value.input
        addFunction(inputValue, id)
    }

    return (
        <Form
            onSubmit={onSubmit}
            render={({handleSubmit}) => (
                <form onSubmit={handleSubmit}>
                    <FormWrapper>
                        <Field
                            name="input"
                            component="input"
                            autoFocus
                            type="text"
                        />
                        <button type="submit">Add</button>
                    </FormWrapper>
                </form>
            )}
        />
    )
}


const FormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  
  & input {
    max-width: 100%;
    margin-bottom: 4px;
    line-height: 24px;
    font-size: 16px;
    border: 1px solid ${variables.defaultBlack};
    border-radius: 3px;
  }
  
  & button {
    width: 100%;
    background: ${variables.defaultYellow};
    border: 1px solid ${variables.defaultBlack};
    border-radius: 3px;
    opacity: .6;
    
    &:hover {
      opacity: 1;
    }
  }
`