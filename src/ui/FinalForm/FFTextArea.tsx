import React from 'react'
import {Form, Field} from 'react-final-form'
import {InputValueType} from '../../dto/types'

type Props = {
    description: string
    addFunction: (value: InputValueType) => void
    maxLength?: string
}

export function FFTextArea({description, addFunction, maxLength}: Props) {
    const onSubmit = (value: InputValueType) => {
        addFunction(value)
    }
    return (
        <Form
            onSubmit={onSubmit}
            render={({handleSubmit}) => (
                <form onSubmit={handleSubmit}>
                    <Field
                        name="input"
                        component="textarea"
                        autoFocus
                        defaultValue={description}
                        maxLength={maxLength ?? "100"}
                    />
                    <button type="submit">OK</button>
                </form>
            )}
        />
    )
}