import React, {ChangeEvent} from 'react'
import {Form, Field} from 'react-final-form'
import {
    EStatus,
    DONE,
    IN_PROGRESS,
    TESTING,
    TO_DO,
    WILL_NEVER_HAPPENED
} from '../../dto/constants'

type Props = {
    status: string
    changeStatus: (e: ChangeEvent<HTMLSelectElement>) => void
}

export function FFSelect({status, changeStatus}: Props) {
    const getStatus = EStatus[Number(status) - 1]
    return (
        <Form
            onSubmit={() => console.log(WILL_NEVER_HAPPENED)}
            render={({handleSubmit}) => (
                <form onSubmit={handleSubmit}>
                    <Field
                        name="status"
                        component="select"
                        //@ts-ignore
                        defaultValue={EStatus[getStatus]}
                        onChange={(e: ChangeEvent<HTMLSelectElement>) => changeStatus(e)}
                    >
                        <option value={EStatus[TO_DO]}>{TO_DO}</option>
                        <option value={EStatus[IN_PROGRESS]}>{IN_PROGRESS}</option>
                        <option value={EStatus[TESTING]}>{TESTING}</option>
                        <option value={EStatus[DONE]}>{DONE}</option>
                    </Field>
                </form>
            )}
        />
    )
}
