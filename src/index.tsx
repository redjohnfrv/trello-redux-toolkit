import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import reportWebVitals from './reportWebVitals'
import {PersistGate} from 'redux-persist/integration/react'
import {Provider} from 'react-redux'
import { store, storePersist } from './redux/store'

ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
          <PersistGate loading={null} persistor={storePersist}>
              <App />
          </PersistGate>
      </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)

reportWebVitals()
