import React, {useEffect} from 'react'
import './assets/styles/default.scss'
import {Field, Form} from 'react-final-form'
import {DEFAULT_AUTHOR_NAME} from './dto/constants'
import {Board} from './components/Board/Board'
import {Popup} from './ui/Popup'
import {useSwitcher} from './hooks/useSwitcher'
import {useDispatch, useSelector} from 'react-redux'
import {selectUser} from './redux/store/user/userSelector'
import {changeUser} from './redux/store/user/userSlice'

function App() {
    const showPopup = useSwitcher()
    const dispatch = useDispatch()
    const userName = useSelector(selectUser).input

    const onSubmit = (value: string) => {
        showPopup.off()
        dispatch(changeUser(value))
    }
    useEffect(() => {
        if (userName === DEFAULT_AUTHOR_NAME || !userName) showPopup.on()
    }, [showPopup, userName])

    return (
        <>
            <Board />
            {showPopup.isOn &&
            <Popup popupClose={() => showPopup.off()}>
                <Form
                    onSubmit={onSubmit}
                    render={({handleSubmit}) => (
                        <form onSubmit={handleSubmit}>
                            <Field
                                name="input"
                                component="input"
                                autoFocus
                                type="text"
                            />
                            <button type="submit">Add</button>
                        </form>
                    )}
                />
            </Popup>}
        </>
    )
}

export default App
