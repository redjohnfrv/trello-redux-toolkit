import {useSelector} from 'react-redux'
import {selectTasks} from '../redux/store/task/selectors'

export const useFindTask = (taskId: string) => {
    const tasks = useSelector(selectTasks)
    return tasks.find(task => task.id === taskId)!
}