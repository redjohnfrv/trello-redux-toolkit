import {useSelector} from 'react-redux'
import {selectComments} from '../redux/store/comment/selectors'

export const useFindComments = (taskId: string) => {
    const comments = useSelector(selectComments)
    return comments.filter(comment => comment.taskId === taskId)
}